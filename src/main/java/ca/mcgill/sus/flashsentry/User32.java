package ca.mcgill.sus.flashsentry;

import com.sun.jna.Native;

public interface User32 extends com.sun.jna.platform.win32.User32 {
	User32 INSTANCE = (User32) Native.loadLibrary("user32.dll", User32.class);
	public void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);
}
