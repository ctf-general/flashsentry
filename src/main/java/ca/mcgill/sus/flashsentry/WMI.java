package ca.mcgill.sus.flashsentry;

import java.util.ArrayList;
import java.util.List;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.EnumVariant;
import com.jacob.com.Variant;

public class WMI {
	
	ActiveXComponent wmi = new ActiveXComponent("winmgmts:\\\\localhost\\root\\CIMV2"); 
	
	public WMIResult execQueryOne(String query) {
		Dispatch result; 
		try {
			result = new EnumVariant(wmi.invoke("ExecQuery", new Variant(query)).toDispatch()).nextElement().toDispatch();
		} catch (Exception e) {
			return null;
		}
		return new WMIResult(result);
	}
	
	public WMIResult execQueryOne(String table, String... columns) {
		StringBuilder query = new StringBuilder("SELECT ");
		for (int i = 0; i < columns.length; i++) {
			query.append(columns[i]).append(i < columns.length - 1 ? ", " : " ");
		}
		query.append("FROM ").append(table);
		return this.execQueryOne(query.toString());
	}
	
	public List<WMIResult> execQuery(String query) {
		List<WMIResult> out = new ArrayList<>();
		EnumVariant variants = new EnumVariant(wmi.invoke("ExecQuery", new Variant(query)).toDispatch());
		for (Dispatch result = null; variants.hasMoreElements();) {
			result = variants.nextElement().getDispatch();
			out.add(new WMIResult(result));
		}
		return out;
	}
	
	public List<WMIResult> execQuery(String table, String... columns) {
		StringBuilder query = new StringBuilder("SELECT ");
		for (int i = 0; i < columns.length; i++) {
			query.append(columns[i]).append(i < columns.length - 1 ? ", " : " ");
		}
		query.append("FROM ").append(table);
		return this.execQuery(query.toString());
	}
	
	public static class WMIResult {
		private final Dispatch dispatch;
		private WMIResult(Dispatch dispatch) {
			this.dispatch = dispatch;
		}
		public Variant get(String name) {
			return Dispatch.call(dispatch, name);
		}
		public String getAsString(String name) {
			try {
				return this.get(name).toString();
			} catch (Exception e) {
				return null;
			}
		}
		public int getAsInt(String name) {
			try {
				return this.get(name).changeType(Variant.VariantInt).getInt();
			} catch (Exception e) {
				return -1;
			}
		}
		public long getAsLong(String name) {
			try {
				return this.get(name).changeType(Variant.VariantLongInt).getLong();
			} catch (Exception e) {
				return -1;
			}
		}
		public boolean getAsBoolean(String name) {
			try {
				return this.get(name).changeType(Variant.VariantBoolean).getBoolean();
			} catch (Exception e) {
				return false;
			}
		}
	}
	
	private WMI() {
	}

	private static WMI instance;
	public static synchronized WMI getInstance() {
		if (instance == null) instance = new WMI();
		return instance;
	}
	
}
