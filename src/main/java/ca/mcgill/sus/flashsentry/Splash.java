package ca.mcgill.sus.flashsentry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;

public class Splash extends JFrame {
	private static final long serialVersionUID = 9113133335401403577L;
	private final BufferedImage sadUsb;
	private final int timeOut = 60;
	private int timer = 0;
	private final String message = "Don't forget your key!!";

	public Splash() {
		super("Flash Sentry");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setAlwaysOnTop(true);
		this.setUndecorated(true);
		BufferedImage sadUsb;
		try {
			sadUsb = ImageIO.read(Main.class.getResourceAsStream("graphics/sadusb.png"));
		} catch (IOException e) {
			sadUsb = null;
		}
		this.sadUsb = sadUsb;
	}
	
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible) {
			this.createBufferStrategy(3);
			//start the music
			try (AudioInputStream inputStream = AudioSystem.getAudioInputStream(new BufferedInputStream(SoundTest.class.getResourceAsStream("sounds/chime.wav")))) {
				Clip clip = AudioSystem.getClip();
		        clip.open(inputStream);
		        clip.loop(Clip.LOOP_CONTINUOUSLY);
			} catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
				e.printStackTrace();
			}
			//maximize window
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			this.setBounds(ge.getDefaultScreenDevice().getDefaultConfiguration().getBounds());
			this.setExtendedState(MAXIMIZED_BOTH);
			//spam the enter key until the logoff screen cancels
			new Thread("Enter Key") {
				@Override
				public void run() {
					for (;;) {
						User32.INSTANCE.keybd_event((byte) 0x0D,(byte)0x1C,0,0);
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}.start();
			//spam volume up key to make chime louder
			new Thread("Volume Up") {
				@Override
				public void run() {
					for (int i = 0; i < 400; i++) {
						User32.INSTANCE.keybd_event((byte) 0xAF,(byte) 0x0,0,0);
					}
					while (Main.forgotUSB() && timer < timeOut) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						timer += 1;
						Splash.this.repaint();
					}
					//file report with server here
					try {
						Runtime.getRuntime().exec("logoff console").waitFor();
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
					System.exit(0);
				}
			}.start();
		}
	}
	
	@Override
	public void paint(Graphics graphics) {
		super.paint(graphics);
		Rectangle bounds = this.getBounds();
		Graphics2D g = (Graphics2D) graphics;
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, bounds.width, bounds.height);
		g.setColor(Color.BLACK);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setFont(FontManager.getInstance().getFont("nhg-bold.ttf").deriveFont(80f));
		g.drawString(this.message, bounds.width / 2 - g.getFontMetrics().stringWidth(this.message) / 2, 200);
		g.drawImage(sadUsb, bounds.width / 2 - sadUsb.getWidth() / 2, bounds.height / 2 - sadUsb.getHeight() / 2, null);
		String logoffTimeout = "You will be logged off in " + (timeOut - timer) + " seconds";
		g.drawString(logoffTimeout, bounds.width / 2 - g.getFontMetrics().stringWidth(logoffTimeout) / 2, 900);
	}
	
}
