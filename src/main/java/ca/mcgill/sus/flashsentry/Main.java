package ca.mcgill.sus.flashsentry;

import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import ca.mcgill.sus.flashsentry.Main.DiskInfo.DiskType;
import ca.mcgill.sus.flashsentry.WMI.WMIResult;
import sun.misc.Signal;
import sun.misc.SignalHandler;

class Main {
    static final String WINDOW_MODIFIED = "windowModified";
	private static final WMI wmi = WMI.getInstance();


    Main() {
    	try {
    		TrayIcon tray = new TrayIcon(ImageIO.read(Main.class.getResourceAsStream("graphics/tray.png")));
    		tray.setToolTip("Flash Sentry");
			SystemTray.getSystemTray().add(tray);
			tray.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println(e);
				}
			});
		} catch (AWTException | IOException e) {
			e.printStackTrace();
		}
        Signal.handle(new Signal("TERM"), new SignalHandler() {
            @Override
            public void handle(Signal arg0) {
        		if (forgotUSB()) {
        			Splash splash = new Splash();
        			splash.setVisible(true);
        		}
            }
        });

    }

    protected static void handleQuit() {

		Splash splash = new Splash();
		splash.setVisible(true);
    }

    public static void main(String[] args) {
        // start the GUI on the EDT
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main();
            }
        });
    }
    
	public static boolean forgotUSB() {
		boolean forgottenUSB = false;
		for (WMIResult r : wmi.execQuery("Win32_LogicalDisk", "Name", "VolumeName", "Size", "FreeSpace", "DriveType")) {
			DiskInfo disk = new DiskInfo();
			disk.mountPoint = r.getAsString("Name");
			disk.volumeName = r.getAsString("VolumeName");
			disk.capacityB = r.getAsLong("Size");
			disk.freeB = r.getAsLong("FreeSpace");
			disk.type = DiskType.forWin32Code(r.getAsInt("DriveType"));
			if (disk.type == DiskType.REMOVABLE && disk.capacityB > 0) {
				System.out.println(disk);
				forgottenUSB = true;
			}
		}
		return forgottenUSB;
	}

	public static class DiskInfo {
		public String mountPoint, volumeName;
		public long capacityB, freeB;
		public DiskType type;
		
		@Override
		public String toString() {
			return String.format("DiskInfo [mountPoint=%s, volumeName=%s, capacityB=%s, freeB=%s, type=%s]", mountPoint, volumeName, capacityB, freeB, type);
		}

		public static enum DiskType {
			UNKNOWN(0), NO_ROOT(1), REMOVABLE(2), LOCAL(3), NETWORK(4), CD(5), RAM(6);
			public int win32Code;
			private DiskType(int win32Code) {
				this.win32Code = win32Code;
			}
			public static DiskType forWin32Code(int code) {
				for (DiskType dt : DiskType.values()) {
					if (dt.win32Code == code) return dt;
				}
				return UNKNOWN;
			}
		}
	}
}
